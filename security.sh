#!/bin/bash

echo -e "\033[1mDésactivation de la bannière serveur\033[0m"
#Désactivation de la bannière serveur
echo -e "ServerTokens Prod" >> /etc/httpd/conf/httpd.conf

#Redémarrage du service pour activer la nouvelle configuration
systemctl restart httpd


echo -e "\033[1mSauvegarde de l'ancienne configuration SSH\033[0m"
#Sauvegarde de l'ancienne configuration SSH
date_format=`date +%Y_%m_%d:%H:%M:%S`
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config_$date_format

echo -e "\033[1mConfiguration du nouveau port\033[0m"
#Configuration du nouveau port SSH
sed -i "17d" /etc/ssh/sshd_config
sed -i "17iPort 33000" /etc/ssh/sshd_config
semanage port -a -t ssh_port_t -p tcp 33000

echo -e "\033[1mOuverture du port sur le pare-feu\033[0m"
#Ouverture du port sur le pare-feu
firewall-cmd --add-port=33000/tcp --permanent
firewall-cmd --reload
firewall-cmd --remove-service=ssh --permanent
firewall-cmd --reload

#Redémarrage du service pour activer la configuration
systemctl restart sshd
