#!/bin/bash

# Configuration Postfix
echo "Sauvegarde de la configuration actuelle..."
mv /etc/postfix/main.cf /etc/postfix/main.cf_old

hostname=`cat /etc/hostname`

#on cherche l'ip de la machine
ip_=$(hostname -I)
ip=$(echo $ip_)

#calcul de l'ip du réseau
mask=$(ifconfig | grep -w inet |grep -v 127.0.0.1| awk '{print $4}' | cut -d ":" -f 2)
ipNet=$(ipcalc -n "$ip" "$mask" | cut -c9-27)
maskNet=$(ipcalc -p "$ip" "$mask" | cut -c8-9)

network="$ipNet/$maskNet"


echo -e "myhostname = mail.$hostname\nmyorigin = $hostname\n## Uncomment and Set inet_interfaces to all ##\ninet_interfaces = all\n## Change to all ##\ninet_protocols = all\n## Comment ##\n#mydestination = $hostname, localhost.$hostname, localhost\n##- Uncomment ##\nmydestination = $hostname, localhost.$hostname, localhost, $hostname\n## Uncomment and add IP range ##\nmynetworks = $network, 127.0.0.0/8\n## Uncomment ##\nhome_mailbox = Maildir/" >> /etc/postfix/main.cf
echo "Configuration Effectuée avec succès, Restart de Postfix ..."
systemctl restart postfix

echo "Postfix restarted."
