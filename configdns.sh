#!/bin/bash

read -p "Entrez votre nom d'utilisateur : " user
read -p "Entrez votre nom de domaine : " domaine
#on vérifie si un argument est présent ou non
if [ -z "$user" ] ; then
	echo "Veuillez ajouter votre nom d'utilisateur après la commande."
	exit
elif [ -z "$domaine" ] ; then
	echo "Veuillez ajouter votre nom de domaine après la commande."
else
	#suppression de l'interface bridge virtuelle
	ifconfig virbr0 down
	#changement du nom d'hôte principal
	host="$domaine"
	hostnamectl set-hostname "$host"
	#on cherche l'ip de la machine
	ip_=$(hostname -I | cut -d ' ' -f 1)
	ip=$(echo $ip_)

	#calcul de l'ip du réseau
	mask=$(ifconfig | grep -w inet |grep -v 127.0.0.1| awk '{print $4}' | cut -d ":" -f 2)
	ipNet=$(ipcalc -n "$ip" "$mask" | cut -c9-27)
	maskNet=$(ipcalc -p "$ip" "$mask" | cut -c8-9)

	network="$ipNet/$maskNet"
	echo "$network"
	#on modifie les fichiers de configuration
	sed -i "11,12d" /etc/named.conf
	sed -i "11i	//listen-on port 53 { 127.0.0.1; };\n	//listen-on-v6 port 53 { ::1; };" /etc/named.conf
	sed -i "19d" /etc/named.conf
	sed -i "19i	allow-query { localhost;$network; };" /etc/named.conf
	echo -e "//forward zone\nzone \"$host\" IN {\n\ttype master;\n\tfile \"$host.db\";\n\tallow-update { none; };\n\tallow-query { any; };\n};" >> /etc/named.conf

	echo -e "\$TTL 86400\n@ IN SOA dns-primary.$host. admin.$host. (\n\t2020011800 ;Serial\n\t3600 ;Refresh\n\t1800 ;Retry\n\t604800 ;Expire\n\t86400 ;Minimum TTL\n)\n@ IN NS dns-primary.$host.\ndns-primary IN A $ip\n\n$host. IN MX 10 mail.$host.\n\nwww IN A $ip\nmail IN A $ip" >> /var/named/"$host".db

	#vérification des fichiers de configuration
	named-checkconf /etc/named.conf
	named-checkzone $host /var/named/$host.db

	#on redémarre le service DNS
	systemctl restart named

	#configuration du firewall
	firewall-cmd --add-service=dns --zone=public --permanent
	firewall-cmd --reload

	#modification du dns actuel
	rm /etc/resolv.conf
	echo -e "search $host\nnameserver $ip" >> /etc/resolv.conf

	echo -e "$ip $host" >> /etc/hosts
fi
