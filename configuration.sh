#!/bin/bash

read -p "Entrez votre nom d'utilisateur : " user
read -p "Entrez votre nom de domaine : " domaine
read -s -p "Entrez votre mot de passe utilisateur : " pass; echo
read -s -p "Entrez le mot de passe qui sera utilisé dans MYSQL : " passN; echo
#on vérifie si les 3 variables est présent ou non
if [ -z "$user" ] ; then
	echo "Veuillez remplir tous les champs."
	exit
elif [ -z "$domaine" ] ; then
	echo "Veuillez remplir tous les champs."
	exit
elif [ -z "$pass" ] ; then
	echo "Veuillez remplir tous les champs."
	exit
elif [ -z "$passN" ] ; then
	echo "Veuillez remplir tous les champs"
	exit
else
	#Configuration du DNS
	echo -e "\033[1mConfiguration du DNS\033[0m"

	#suppression de l'interface bridge virtuelle
	ifconfig virbr0 down

	#changement du nom d'hôte principal
	host="$domaine"
	hostnamectl set-hostname "$host"

	#on cherche l'ip de la machine
	ip_=$(hostname -I | cut -d ' ' -f 1)
	ip=$(echo $ip_)

	#calcul de l'ip du réseau
	mask=$(ifconfig | grep -w inet |grep -v 127.0.0.1| awk '{print $4}' | cut -d ":" -f 2)
	ipNet=$(ipcalc -n "$ip" "$mask" | cut -c9-27)
	maskNet=$(ipcalc -p "$ip" "$mask" | cut -c8-9)

	network="$ipNet/$maskNet"
	echo "$network"
	#on modifie les fichiers de configuration
	sed -i "11,12d" /etc/named.conf
	sed -i "11i	//listen-on port 53 { 127.0.0.1; };\n	//listen-on-v6 port 53 { ::1; };" /etc/named.conf
	sed -i "19d" /etc/named.conf
	sed -i "19i	allow-query { localhost;$network; };" /etc/named.conf
	echo -e "//forward zone\nzone \"$host\" IN {\n\ttype master;\n\tfile \"$host.db\";\n\tallow-update { none; };\n\tallow-query { any; };\n};" >> /etc/named.conf
	echo -e "\$TTL 86400\n@ IN SOA dns-primary.$host. admin.$host. (\n\t2020011800 ;Serial\n\t3600 ;Refresh\n\t1800 ;Retry\n\t604800 ;Expire\n\t86400 ;Minimum TTL\n)\n@ IN NS dns-primary.$host.\ndns-primary IN A $ip\n\n$host. IN MX 10 mail.$host.\n\nwww IN A $ip\nmail IN A $ip" >> /var/named/"$host".db

	#vérification des fichiers de configuration
	named-checkconf /etc/named.conf
	named-checkzone $host /var/named/$host.db

	#on redémarre le service DNS
	systemctl restart named

	#configuration du firewall
	firewall-cmd --add-service=dns --zone=public --permanent
	firewall-cmd --reload

	#modification du dns actuel
	rm /etc/resolv.conf
	echo -e "search $host\nnameserver $ip" >> /etc/resolv.conf

	echo -e "$ip $host" >> /etc/hosts


	#Configuration serveur mail
	echo -e "\033[1mConfiguration du serveur mail\033[0m"

	# Configuration Postfix
	echo "Sauvegarde de la configuration actuelle..."
	mv /etc/postfix/main.cf /etc/postfix/main.cf_old

	hostname=`cat /etc/hostname`

	#on cherche l'ip de la machine
	ip_=$(hostname -I)
	ip=$(echo $ip_)

	#calcul de l'ip du réseau
	mask=$(ifconfig | grep -w inet |grep -v 127.0.0.1| awk '{print $4}' | cut -d ":" -f 2)
	ipNet=$(ipcalc -n "$ip" "$mask" | cut -c9-27)
	maskNet=$(ipcalc -p "$ip" "$mask" | cut -c8-9)

	network="$ipNet/$maskNet"


	echo -e "myhostname = mail.$hostname\nmyorigin = $hostname\n## Uncomment and Set inet_interfaces to all ##\ninet_interfaces = all\n## Change to all ##\ninet_protocols = all\n## Comment ##\n#mydestination = $hostname, localhost.$hostname, localhost\n##- Uncomment ##\nmydestination = $hostname, localhost.$hostname, localhost, $hostname\n## Uncomment and add IP range ##\nmynetworks = $network, 127.0.0.0/8\n## Uncomment ##\nhome_mailbox = Maildir/" >> /etc/postfix/main.cf
	echo "Configuration Effectuée avec succès, Restart de Postfix ..."
	systemctl restart postfix
	echo "Postfix restarted."

	#Configuration de MYSQL
	echo -e "\033[1mConfiguration de MYSQL\033[0m"

	#Modification du mot de passe root dans MYSQL
	mysql -e "SET PASSWORD FOR root@localhost = PASSWORD('$passN');FLUSH PRIVILEGES;" 
	printf "$pass\n n\n n\n y\n y\n y\n y\n" | sudo mysql_secure_installation 1>/bin/null

	#Vérification du status de la commande afin d'afficher un message d'erreur
	if [ "$?" -gt 0 ] ; then
		echo "Une erreur est survenue lors de la configuration."
	else
		echo "La configuration est terminée."
	fi
	#Configuration de la sécurité
	echo -e "\033[1mConfiguration de la sécurité\033[0m"

	echo -e "Désactivation de la bannière serveur"
	#Désactivation de la bannière serveur
	echo -e "ServerTokens Prod" >> /etc/httpd/conf/httpd.conf

	#Redémarrage du service pour activer la nouvelle configuration
	systemctl restart httpd


	echo -e "Sauvegarde de l'ancienne configuration SSH"
	#Sauvegarde de l'ancienne configuration SSH
	date_format=`date +%Y_%m_%d:%H:%M:%S`
	sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config_$date_format

	echo -e "Configuration du nouveau port"
	#Configuration du nouveau port SSH
	sed -i "17d" /etc/ssh/sshd_config
	sed -i "17iPort 33000" /etc/ssh/sshd_config
	semanage port -a -t ssh_port_t -p tcp 33000

	echo -e "Ouverture du port sur le pare-feu"
	#Ouverture du port sur le pare-feu
	firewall-cmd --add-port=33000/tcp --permanent
	firewall-cmd --reload
	firewall-cmd --remove-service=ssh --permanent
	firewall-cmd --reload

	#Redémarrage du service pour activer la configuration
	systemctl restart sshd

fi
