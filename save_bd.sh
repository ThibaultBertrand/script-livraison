#!/bin/bash

read -p "Entrez votre mot de passe MYSQL : " pass
#on vérifie si un argument est présent ou non
if [ -z "$pass" ] ; then
	echo "Veuillez ajouter votre mot de passe MYSQL."
	exit
else
        # On liste nos bases de données
        LISTEBDD=$( echo 'show databases' | mysql -u root --password=$pass)
        # Date à laquel sera fait la sauvegarde
        DATE=`date +%y_%m_%d`
        for BDD in $LISTEBDD ; do
        	# Exclusion des BDD performance_schema, information_schema , mysql et Database
        	if [ $BDD != "performance_schema" ] && [ $BDD != "information_schema" ] && [ $BDD != "mysql" ] ; then
                	# Emplacement du dossier ou nous allons stocker les bases de données, un dossier par base de données
                	CHEMIN=/save
                	# Si le repertoire de la BDD dans $CHEMIN n'existe pas, on le cree
                	if [ ! -d "$CHEMIN" ];then
                        	echo "Un dossier save a été créé à la racine."
                        	mkdir -p $CHEMIN/
                	fi
               		# On backup notre base de donnees
                	mysqldump -u root --password="$pass" --all-databases > $BDD > "$CHEMIN/$DATE"_"$BDD.sql"
                	echo "Sauvegarde de la base de donnees $BDD.sql ";
        	fi
	done
fi
