#!/bin/bash

# Update system
echo -e "\033[1mMise à jour du système\033[0m"
#yum upgrade -y

# Install apache
echo -e "\033[1mInstallation du serveur apache\033[0m"
dnf install httpd httpd-tools -y
systemctl enable httpd
systemctl start httpd

# Update firewall
echo -e "\033[1mMise à jour du firewall\033[0m"
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload

# Install bdd
echo -e "\033[1mInstallation de la base de donnée\033[0m"
dnf install mariadb-server mariadb -y
systemctl start mariadb
systemctl enable mariadb

# Install PHP 7
echo -e "\033[1mInstallation de PHP\033[0m"
dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
yum install php php-{gd,pdo,xml,mbstring,zip,mysqlnd,opcache,json,intl} -y

# Install PhpMyAdmin
wget https://files.phpmyadmin.net/phpMyAdmin/5.1.1/phpMyAdmin-5.1.1-all-languages.tar.gz
tar -zxvf phpMyAdmin-5.1.1-all-languages.tar.gz 1> /bin/null
rm phpMyAdmin-5.1.1-all-languages.tar.gz
mv phpMyAdmin-5.1.1-all-languages /var/www/html/phpMyAdmin/

# Install DNS
echo -e "\033[1mInstallation du DNS\033[0m"
dnf install bind bind-utils -y
dnf install telnet -y
# Install SMTP
echo -e "\033[1mInstallation du SMTP\033[0m"
dnf install postfix -y
dnf install mailx -y
dnf install openssl -y

# Install IMAP
echo -e "\033[1mInstallation IMAP\033[0m"
dnf install dovecot -y

# Install Cockpit
echo -e "\033[1mInstallation de Cockpit\033[0m"
dnf install cockpit cockpit-pcp -y 

echo -e "\033[1mDémarrage des services\033[0m"
systemctl start php-fpm
systemctl start named
systemctl start postfix
systemctl start dovecot
systemctl start cockpit
setsebool -P httpd_execmem 1
systemctl restart httpd
